FROM arm32v7/node:10.19.0
RUN apt update && apt install -y openjdk-8-jdk
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
