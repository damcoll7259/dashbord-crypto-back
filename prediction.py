import psycopg2
from datetime import datetime as dt
import numpy as np
import datetime
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Lasso
from sklearn.linear_model import Ridge
from sklearn.linear_model import LogisticRegression


conn = psycopg2.connect(
    host="localhost",
    database="postgres",
    user="jonny",
    password="44719")

cur = conn.cursor()

nb_prediction = 24

def getAllCrypto() :
    cur.execute("SELECT id FROM crypto where active = true;")
    rows = cur.fetchall()
    return rows

def predicateAll(name_crypto) :
    if (name_crypto == "EUR") :
        return
    cur.execute("SELECT * FROM prix where name_crypto = '"+name_crypto+"' order by date_prix;")
    # get all rows from table prix and set object to variable
    rows = cur.fetchall()
    # create dataset from rows
    X = []
    y = []
    for row in rows:
        #convert row[0] to number in variable date
        date = 100000*row[0].year + 1000*row[0].month + 10*row[0].day + row[0].hour 
        X.append([date])
        y.append(float(row[1]))
    predicateOne(name_crypto, X, y, nb_prediction)
    predicateOne(name_crypto, X, y, nb_prediction, 2)
    predicateOne(name_crypto, X, y, nb_prediction, 3)
    

def predicateOne (crypto, X, y, index_max , index_algo = 1) :
    print("Prediction for "+crypto+" with "+str(index_max)+" steps and index_algo: " + str(index_algo) )
    algo = LinearRegression()
    if (index_algo == 1) :
        algo = LinearRegression()
    elif (index_algo == 2) :
        algo = Lasso()
    elif (index_algo == 3) :
        algo = Ridge()
    algo.fit(X, y)
    algo.score(X, y)
    algo.coef_
    algo.intercept_

    X_prev = []
    i = 0
    while i < index_max :
        date = dt.now() + datetime.timedelta(hours=1 + i)
        date = 100000*date.year + 1000*date.month + 10*date.day + date.hour
        X_prev.append([date])
        i += 1
    y_prev = algo.predict(np.array(X_prev))
    i = 0
    while i < index_max :
        now = dt.now() + datetime.timedelta(hours=1 + i)
        query = "INSERT INTO prediction (name_crypto, name_currency, date_prix, value, algo, index) VALUES (%s, 'EUR', %s, %s, %s, %s);" 
        cur.execute(query, (crypto, str(now), y_prev[i], index_algo, i))
        conn.commit()
        i += 1


cryptos = getAllCrypto()
i = 0
while i < len(cryptos) :
    predicateAll(cryptos[i][0])
    i += 1
conn.close()