package com.collado.dashbordcrypto.crypto.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class CryptoDTO {

	private String name;
	
	@NotBlank
	private String ID;
	
}
