package com.collado.dashbordcrypto.crypto.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import com.collado.dashbordcrypto.crypto.dto.CryptoDTO;
import com.collado.dashbordcrypto.crypto.entity.CryptoEntity;
import com.collado.dashbordcrypto.crypto.repository.CryptoRepository;
import com.collado.dashbordcrypto.utils.MapperService;


@Slf4j
@Service
public class CryptoService extends MapperService<CryptoEntity, CryptoDTO> {


	@Autowired
	private CryptoRepository cryptoRepository;

	@Autowired
	private Validator validator;

	public List<CryptoDTO> getCrypto() {
		return StreamSupport.stream(cryptoRepository.findAll().spliterator(), false).map(this::convertToDTO)
				.collect(Collectors.toList());
	}

	public List<CryptoEntity> getCryptoEntity() {
		return StreamSupport.stream(cryptoRepository.findAll().spliterator(), false).collect(Collectors.toList());
	}

	public void putCrypto(CryptoDTO _crypto) {
		Set<ConstraintViolation<CryptoDTO>> violations = validator.validate(_crypto);
		if (!violations.isEmpty()) {
			throw new ConstraintViolationException(violations);
		}
		cryptoRepository.save(convertToEntity(_crypto));
	}

	public CryptoEntity getCrypto(String ID) {
		try {
			Optional<CryptoEntity> crypto =  cryptoRepository.getOneByID(ID);
			return crypto.isPresent() ? crypto.get() : null;
		}catch (Exception e) {
			System.err.println(ID);
			return null;
		}
	}

	public CryptoEntity getOrCreateEntity(String ID) {
		CryptoEntity crypto = getCrypto(ID);
		if (crypto == null) {
			crypto = new CryptoEntity();
			crypto.setID(ID);
			log.info("CryptoEntity created: ", crypto.getID());
			cryptoRepository.save(crypto);
		}
		return crypto;
	}

	public void deleteCrypto(String name) {
		cryptoRepository.deleteById(name);
	}


}
