package com.collado.dashbordcrypto.crypto.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.collado.dashbordcrypto.crypto.entity.CryptoEntity;

public interface CryptoRepository extends CrudRepository<CryptoEntity, String>{

	Optional<CryptoEntity> getOneByID(String ID);
	
}
