package com.collado.dashbordcrypto.crypto.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.collado.dashbordcrypto.crypto.dto.CryptoDTO;
import com.collado.dashbordcrypto.crypto.service.CryptoService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/cryto")
public class CryptoController {

	@Autowired
	private CryptoService cryptoService;
	
	@GetMapping
	public List<CryptoDTO> getCrypto() {
		return cryptoService.getCrypto();
	}

	@PutMapping
	public ResponseEntity<String> putCrypto(@Valid @RequestBody CryptoDTO _crypto) {
		cryptoService.putCrypto(_crypto);
		return ResponseEntity.ok("valid");
	}
	
	@DeleteMapping(path = "/{name}")
	public ResponseEntity<String> deleteCrypto(@PathVariable String name){
		cryptoService.deleteCrypto(name);
		return ResponseEntity.ok("valid");
	}
	
}
