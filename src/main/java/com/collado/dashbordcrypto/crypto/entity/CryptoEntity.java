package com.collado.dashbordcrypto.crypto.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity(name = "CRYPTO")
public class CryptoEntity {
	
	@Column(name = "NAME")
	private String name;
	
	@Id
	@Column(name = "ID", nullable = false)
	private String ID;
	
	@Column(name = "ACTIVE")
	private boolean active = false;
}
