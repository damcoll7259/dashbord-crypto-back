package com.collado.dashbordcrypto.prediction.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.collado.dashbordcrypto.crypto.entity.CryptoEntity;

import lombok.Data;

@Data
@Embeddable
public class Key implements Serializable{
    
    @ManyToOne
	@JoinColumn(name = "NAME_CRYPTO")
	private CryptoEntity crypto;
	
	@ManyToOne
	@JoinColumn(name = "NAME_CURRENCY")
	private CryptoEntity currency;
	
	@Column(name = "DATE_PRIX", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date_prix;

    @Column(name = "ALGO")
    private int algo;   

	@Column(name = "INDEX")
    private int index;   
}
