package com.collado.dashbordcrypto.prediction.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import lombok.Data;

/**
 * PredictionEntity
 */
@Data
@Entity(name = "PREDICTION")
public class PredictionEntity {

    @EmbeddedId
	private  Key key;
	
	@Column(name = "VALUE", nullable = false)
	private BigDecimal value;
}