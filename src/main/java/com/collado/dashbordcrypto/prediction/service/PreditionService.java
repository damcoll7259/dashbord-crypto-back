package com.collado.dashbordcrypto.prediction.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PreditionService {

  //  @Scheduled(fixedRate = 3600000)
    public void predict() throws IOException {
        log.info("Predicting...");
        // File file = new File(
        // this.getClass().getResource("python/script/prediction.py").getFile());
        String line = "python " + getFile().getAbsolutePath();
        CommandLine cmdLine = CommandLine.parse(line);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);

        DefaultExecutor executor = new DefaultExecutor();
        executor.setStreamHandler(streamHandler);

        int exitCode = executor.execute(cmdLine);
        log.info("Prediction finished with exit code: {}", exitCode);
    }

    private File getFile() {
        File file = null;
        String resource = "prediction.py";
        // this will probably work in your IDE, but not from a JAR
        file = new File(resource);
        return file;
    }

}
