package com.collado.dashbordcrypto.utils;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;


public class MapperService<Entity, Dto> {

	@Autowired
	protected ModelMapper modelMapper;
	
	private Class<Dto> dtoClass;
	
	private Class<Entity> entityClass;
	
	protected Dto convertToDTO(Entity entity) {
		return modelMapper.map(entity, dtoClass);
	}

	protected Entity convertToEntity(Dto dto) {
		return modelMapper.map(dto, entityClass);
	}
	
}
