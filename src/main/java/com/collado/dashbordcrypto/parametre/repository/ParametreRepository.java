package com.collado.dashbordcrypto.parametre.repository;

import com.collado.dashbordcrypto.parametre.entity.ParametreEntity;

import org.springframework.data.repository.CrudRepository;

/**
 * ParametreRepository
 */
public interface ParametreRepository extends CrudRepository<ParametreEntity, String> {

}