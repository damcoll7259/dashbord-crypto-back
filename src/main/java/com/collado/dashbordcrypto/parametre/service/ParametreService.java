package com.collado.dashbordcrypto.parametre.service;

import com.collado.dashbordcrypto.parametre.entity.ParametreEntity;
import com.collado.dashbordcrypto.parametre.repository.ParametreRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParametreService {
    
    @Autowired
    private ParametreRepository parametreRepository;

    public String getParametre(String parametre) {
        if(!parametreRepository.findById(parametre).isPresent())
            parametreRepository.save(new ParametreEntity(parametre, "A Parametrer"));
        return parametreRepository.findById(parametre).get().getValeur();
    }

    public void setParametre(String parametre, String valeur) {
        parametreRepository.save(new ParametreEntity(parametre, valeur));
    }
}
