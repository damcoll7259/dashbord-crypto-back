package com.collado.dashbordcrypto.parametre.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity(name = "parametre")
public class ParametreEntity {
    
    @Id
    @Column(name = "NOM", nullable = false)
    private String nom;

    @Column(name = "VALEUR", nullable = false)
    private String valeur;

}
