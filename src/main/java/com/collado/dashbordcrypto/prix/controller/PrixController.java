package com.collado.dashbordcrypto.prix.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.collado.dashbordcrypto.prix.dto.PrixDTO;
import com.collado.dashbordcrypto.prix.service.PrixService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/prix")
public class PrixController {

	@Autowired
	private PrixService prixService;
	
	@GetMapping
	public List<PrixDTO> getAllPrix(){
		return prixService.getAllPrix();
	}
	
	@PutMapping
	private ResponseEntity<String> putPrix(@RequestBody PrixDTO _prix) {
		prixService.putPrix(_prix);
		return ResponseEntity.ok("valid");
	}
}
