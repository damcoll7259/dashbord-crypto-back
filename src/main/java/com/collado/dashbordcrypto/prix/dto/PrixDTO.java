package com.collado.dashbordcrypto.prix.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class PrixDTO {
	
	private KeyPrixDTO key;
	
	private BigDecimal value;
	
}
