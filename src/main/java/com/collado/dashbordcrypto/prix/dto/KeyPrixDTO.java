package com.collado.dashbordcrypto.prix.dto;

import java.time.LocalDate;

import com.collado.dashbordcrypto.crypto.dto.CryptoDTO;

import lombok.Data;

@Data
public class KeyPrixDTO {
	
	private CryptoDTO crypto;
	
	private LocalDate date_prix;
	
}
