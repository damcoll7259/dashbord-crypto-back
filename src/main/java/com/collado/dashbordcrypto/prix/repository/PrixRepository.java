package com.collado.dashbordcrypto.prix.repository;

import org.springframework.data.repository.CrudRepository;

import com.collado.dashbordcrypto.prix.entity.PrixEntity;
import com.collado.dashbordcrypto.prix.entity.PrixKey;

public interface PrixRepository extends CrudRepository<PrixEntity, PrixKey>{

    

}
