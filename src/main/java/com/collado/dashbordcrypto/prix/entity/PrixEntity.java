package com.collado.dashbordcrypto.prix.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import lombok.Data;

@Data
@Entity(name = "PRIX")
public class PrixEntity {

	@EmbeddedId
	private PrixKey key;
	
	@Column(name = "VALUE", nullable = false)
	private BigDecimal value;
}
