package com.collado.dashbordcrypto.prix.service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.collado.dashbordcrypto.crypto.repository.CryptoRepository;
import com.collado.dashbordcrypto.crypto.service.CryptoService;
import com.collado.dashbordcrypto.parametre.service.ParametreService;
import com.collado.dashbordcrypto.prix.dto.PrixDTO;
import com.collado.dashbordcrypto.prix.entity.PrixEntity;
import com.collado.dashbordcrypto.prix.entity.PrixKey;
import com.collado.dashbordcrypto.prix.repository.PrixRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;

@Slf4j
@Service
public class PrixService {
	private final String URI_PRICE_CONBASE = "https://api.coinbase.com/v2/exchange-rates?currency=EUR";

	@Autowired
	private PrixRepository prixRepository;

	@Autowired
	private CryptoService cryptoService;
	@Autowired
	private ParametreService parametreService;
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ModelMapper modelMapper;

	public List<PrixDTO> getAllPrix() {
		return StreamSupport.stream(prixRepository.findAll().spliterator(), false).map(this::convertToDTO)
				.collect(Collectors.toList());
	}

	public void putPrix(PrixDTO _prix) {
		prixRepository.save(convertToEntity(_prix));
	}

	private PrixDTO convertToDTO(PrixEntity entity) {
		return modelMapper.map(entity, PrixDTO.class);
	}

	private PrixEntity convertToEntity(PrixDTO dto) {
		return modelMapper.map(dto, PrixEntity.class);
	}
	// @Scheduled(cron = "0 * * * * *")
	@Scheduled(fixedRate = 60000)
	public void getPriceFromCoinbase() {
		String update = parametreService.getParametre("update");
		ResponseEntity<JSONObject> retour = restTemplate.getForEntity(URI_PRICE_CONBASE, JSONObject.class);
		if (retour.hasBody()) {
			convertCoinbaseToEntity(retour.getBody());
		}
		if (update.equalsIgnoreCase("oui")) {
			parametreService.setParametre("update", "non");
			getHistoriquePrix();
		}
	}

	@SuppressWarnings("unchecked")
	private void convertCoinbaseToEntity(Object prixObject) {
		Calendar date = Calendar.getInstance();
		// Date date = new Date(Date);
		HashMap<String, Object> prix = (HashMap<String, Object>) ((HashMap<String, Object>) prixObject).get("data");

		String currency = (String) prix.get("currency");
		HashMap<String, String> rates = (HashMap<String, String>) prix.get("rates");
		for (String rate : rates.keySet()) {
			BigDecimal value = getValueCrypto(rate, currency);
			PrixKey key = new PrixKey();
			key.setCrypto(cryptoService.getOrCreateEntity(rate));
			key.setCurrency(cryptoService.getOrCreateEntity(currency));
			key.setDate_prix(date.getTime());
			PrixEntity priceEntity = new PrixEntity();
			priceEntity.setKey(key);
			priceEntity.setValue(value); // todo
			if (key.getCrypto() == null || value == null)
				continue;
			prixRepository.save(priceEntity);
		}
	}

	@SuppressWarnings("unchecked")
	private BigDecimal getValueCrypto(String rate, String currency) {
		try {
			String url = "https://api.coinbase.com/v2/prices/" + rate + "-" + currency + "/spot";
			ResponseEntity<JSONObject> retour = restTemplate.getForEntity(url, JSONObject.class);
			return new BigDecimal(((HashMap<String, String>) retour.getBody().get("data")).get("amount"));
		} catch (Exception e) {
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	private BigDecimal getValueCryptoDate(String rate, String currency, Date date) {
		String url = "https://api.coinbase.com/v2/prices/" + rate + "-" + currency + "/spot";
		String urlTemplate = UriComponentsBuilder.fromHttpUrl(url)
        .queryParam("date", "{date}")
        .encode()
        .toUriString();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-hh-mm");
        String today = formatter.format(date);
		Map<String, String> params = new HashMap<>();
		params.put("date", today);
		try {
			ResponseEntity<JSONObject> retour = restTemplate.exchange(urlTemplate, HttpMethod.GET, null, JSONObject.class, params);
			log.info("retour : " + retour.getBody());
			log.info("retour : " + ((HashMap<String, String>) retour.getBody().get("data")).get("amount"));

			return new BigDecimal(((HashMap<String, String>) retour.getBody().get("data")).get("amount"));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private long countHistoCrypto(String crypto){
		return StreamSupport.stream(prixRepository.findAll().spliterator(),false).filter(p -> p.getKey().getCrypto().getID().equals(crypto)).count();
	}

	private List<String> getCryptoValide(){
		return cryptoService.getCryptoEntity().stream().filter(c -> c.isActive()).map(c -> c.getID()).collect(Collectors.toList());
	}
	
	public void getHistoriquePrix() {
		String historique = parametreService.getParametre("historique");
		int nb = historique.equals("A Parametrer") ? 20000 : Integer.parseInt(historique);
		Calendar date = Calendar.getInstance();
		
		for (String crypto : getCryptoValide()){
			Calendar datecrypto  = (Calendar) date.clone();
			for(int i = 0; i < nb && nb > countHistoCrypto(crypto); i++){
				String rate = "EUR";
				datecrypto.add(Calendar.HOUR, -1);
				BigDecimal value = getValueCryptoDate(crypto, rate, datecrypto.getTime());
				PrixKey key = new PrixKey();
				key.setCrypto(cryptoService.getOrCreateEntity(rate));
				key.setCurrency(cryptoService.getOrCreateEntity(crypto));
				key.setDate_prix(date.getTime());
				PrixEntity priceEntity = new PrixEntity();
				priceEntity.setKey(key);
				priceEntity.setValue(value); // todo
				if (key.getCrypto() == null || value == null)
					continue;
				prixRepository.save(priceEntity);
			}
		}

	}
}
