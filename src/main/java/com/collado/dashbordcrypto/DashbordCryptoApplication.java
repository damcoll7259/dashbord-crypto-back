package com.collado.dashbordcrypto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DashbordCryptoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DashbordCryptoApplication.class, args);
	}

}
