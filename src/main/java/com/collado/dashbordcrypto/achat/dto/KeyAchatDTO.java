package com.collado.dashbordcrypto.achat.dto;

import java.time.LocalDate;

import com.collado.dashbordcrypto.crypto.dto.CryptoDTO;

import lombok.Data;

@Data
public class KeyAchatDTO {

	private CryptoDTO crypto;
	
	private LocalDate date_achat;
	
}
