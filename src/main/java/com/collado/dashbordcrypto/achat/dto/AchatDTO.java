package com.collado.dashbordcrypto.achat.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class AchatDTO {

	private KeyAchatDTO key;
	
	private BigDecimal quantity;
	
}
