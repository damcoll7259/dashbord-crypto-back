package com.collado.dashbordcrypto.achat.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.collado.dashbordcrypto.crypto.entity.CryptoEntity;

import lombok.Data;

@Data
@Embeddable
public class AchatKey implements Serializable{

	@ManyToOne
	@JoinColumn(name = "NAME")
	private CryptoEntity crypto;
	
	@Column(name = "DATE_ACHAT", nullable = false)
	private LocalDate date_achat;
	
}
