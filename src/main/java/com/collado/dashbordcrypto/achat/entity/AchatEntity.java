package com.collado.dashbordcrypto.achat.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import lombok.Data;

@Data
@Entity(name = "ACHAT")
public class AchatEntity {

	@EmbeddedId
	private AchatKey key;
	
	@Column(name = "QUANTITY", nullable = false)
	private BigDecimal quantity;
	
}
