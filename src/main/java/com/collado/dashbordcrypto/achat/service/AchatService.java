package com.collado.dashbordcrypto.achat.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.collado.dashbordcrypto.achat.dto.AchatDTO;
import com.collado.dashbordcrypto.achat.entity.AchatEntity;
import com.collado.dashbordcrypto.achat.repository.AchatRepository;
import com.collado.dashbordcrypto.utils.MapperService;

@Service
public class AchatService extends MapperService<AchatEntity, AchatDTO>{

	@Autowired
	private AchatRepository achatRepository;
	
	public List<AchatDTO> getAllAchat() {
		return StreamSupport.stream(achatRepository.findAll().spliterator(), false)
				.map(this::convertToDTO)
				.collect(Collectors.toList());
	}
	
}
