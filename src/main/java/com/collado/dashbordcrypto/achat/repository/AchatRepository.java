package com.collado.dashbordcrypto.achat.repository;

import org.springframework.data.repository.CrudRepository;

import com.collado.dashbordcrypto.achat.entity.AchatEntity;
import com.collado.dashbordcrypto.achat.entity.AchatKey;

public interface AchatRepository extends CrudRepository<AchatEntity, AchatKey> {

}
